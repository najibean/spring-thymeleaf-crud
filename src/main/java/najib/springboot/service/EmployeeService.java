package najib.springboot.service;

import java.util.List;

import org.springframework.data.domain.Page;

import najib.springboot.model.Employee;

public interface EmployeeService {
	List<Employee> getAllEmployees();
	void saveEmployee(Employee employee);
	Employee getEmployeeById(Long id);
	void deleteEmployeeById(long id);
	// PAGINATION and SORTING already covered by this method below
	Page<Employee> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);	
}
